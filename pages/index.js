import {
  Container,
  Tabs,
  TabList,
  Tab,
  TabPanels,
  TabPanel,
} from '@chakra-ui/react';

import V2CheckboxType from '../components/V2CheckboxType';
import V2InvisibleType from '../components/V2InvisibleType';
import EnterPriseCheckboxType from '../components/EnterPriseCheckboxType';
import EnterPriseInvisibleType from '../components/EnterPriseInvisibleType';

export default function Home() {
  return (
    <Container mt="8" maxW="2xl">
      <Tabs variant="enclosed" isLazy>
        <TabList>
          <Tab>v2 - 核取方塊</Tab>
          <Tab>v2 - 隱形標記</Tab>
          <Tab>Enterprise - 核取方塊</Tab>
          <Tab>Enterprise - 隱形標記</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <V2CheckboxType />
          </TabPanel>
          <TabPanel>
            <V2InvisibleType />
          </TabPanel>
          <TabPanel>
            <EnterPriseCheckboxType />
          </TabPanel>
          <TabPanel>
            <EnterPriseInvisibleType />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Container>
  );
}
