import { useEffect, useState } from 'react';
import { Grid, GridItem, Input, Center, Button } from '@chakra-ui/react';

export default function EnterPriseCheckboxType() {
  const [countryCode, setCountryCode] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    const recaptchaValue = window.grecaptcha.enterprise.getResponse();
    console.log('--- google recaptcha tokenx ---');
    console.log(recaptchaValue);

    const response = await fetch(process.env.NEXT_PUBLIC_API_URL, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        google_recaptcha_token: recaptchaValue,
        country_code: countryCode,
        phone_number: phoneNumber,
      }),
    });

    console.log('--- server response ---');
    console.log(await response.json());
  };

  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://www.google.com/recaptcha/enterprise.js';
    script.async = true;
    script.defer = true;
    document.body.appendChild(script);
  }, []);

  return (
    <form onSubmit={handleSubmit}>
      <Center>
        <Grid templateColumns="repeat(5, 1fr)" gap={6}>
          <GridItem colSpan={2}>
            <Input
              placeholder="國碼"
              onChange={(e) => setCountryCode(e.target.value)}
            />
          </GridItem>
          <GridItem colSpan={3}>
            <Input
              type="tel"
              placeholder="手機號碼"
              onChange={(e) => setPhoneNumber(e.target.value)}
            />
          </GridItem>
          <GridItem colSpan={5}>
            <Center>
              <div
                className="g-recaptcha"
                data-sitekey={
                  process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY_ENTERPRISE_CHECKBOX
                }
              ></div>
            </Center>
          </GridItem>
          <GridItem colSpan={5} textAlign="center">
            <Button type="submit" colorScheme="blue">
              發送驗證碼
            </Button>
          </GridItem>
        </Grid>
      </Center>
    </form>
  );
}
